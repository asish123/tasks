@extends('layouts.app')
@section('content')

<!DOCTYPE html>

<h1> This is your list</h1>
@if (Request::is('tasks'))  
<h2><a href="{{action('TaskController@mytasks')}}">All Task</a></h2>
@else
<h2><a href="{{action('TaskController@index')}}">My Task</a></h2>

@endif
<ul> 
@foreach($tasks as $task)
<li>
 <a> {{$task->title}} </a> &nbsp; <a href ="{{route('tasks.edit', $task ->id)}}">Edit </a> &nbsp;
 <a href="{{route('delete', $task->id)}}"> Delete</a> &nbsp;
 
 @if ($task->status == 0)
            
            @can('admin') <a href="{{route('done', $task->id)}}">Mark As done</a>
            @endcan 
            @else
            Done!
            @endif
@endforeach
</ul>

<a href = "{{route('tasks.create')}}"> creat a new tasks </a>
</html>
@endsection