<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;
use Illuminate\Support\Facades\Gate;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    $id = Auth::id();
     $user =User::find($id);
    $tasks = Task::all();
     return view('tasks.index',['tasks' => $tasks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Task();
        $id = 1; 
        $task -> status = 0;
        $task->title = $request->title;
        $task->user_id = $id; 
       $task->save(); 
        return redirect('tasks'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        return view('tasks.edit', ['task' => $task]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::find($id);
        $task->update($request -> all());
       return redirect('tasks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $task = Task::find($id);
        $task->delete(); 
        if (Gate::denies('admin')) {
            abort(403,"You cant delete tasks");
        }
        return redirect('tasks');   
    }

public function done($id)
{
    //only if this task belongs to user 
    if (Gate::denies('admin')) {
        abort(403,"You are not allowed to mark tasks as dome..");
     }          
    $task = Task::findOrFail($id);            
    $task->status = 1; 
    $task->save();
    return redirect('tasks');    
}  

public function mytasks()
    {
        if (Auth::check()) {
            // The user is logged in..
        // if i want to see just my tasks:
        $id= Auth::id();
        $user = User::find($id);
        $tasks = $user->tasks;

        return view('tasks.index', compact('tasks'));
        }
        return redirect()->intended('/home');
    }

}
